package com.br.investimentos.Api.Investimentos.controllers;

import com.br.investimentos.Api.Investimentos.models.Investimento;
import com.br.investimentos.Api.Investimentos.models.SimulacaoCadastro;
import com.br.investimentos.Api.Investimentos.models.SimulacaoResposta;
import com.br.investimentos.Api.Investimentos.models.Usuario;
import com.br.investimentos.Api.Investimentos.security.DetalhesUsuario;
import com.br.investimentos.Api.Investimentos.security.JWTUtil;
import com.br.investimentos.Api.Investimentos.services.SimulacaoService;
import com.br.investimentos.Api.Investimentos.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTest {

    @MockBean
    private SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private JWTUtil jwtUtil;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void init(){
        Usuario usuario = new Usuario(1, "Joao", "joao@joao", "123");
        DetalhesUsuario detalhesUsuario = new DetalhesUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        Mockito.when(usuarioService.loadUserByUsername(Mockito.anyString())).thenReturn(detalhesUsuario);

        Mockito.when(jwtUtil.getUsername(Mockito.anyString())).thenReturn("joao@joao");
        Mockito.when(jwtUtil.tokenValido(Mockito.anyString())).thenReturn(true);
    }

    @Test
    public void deveRealizarSimulacaoComSucesso() throws Exception {
        SimulacaoResposta simulacaoResposta = new SimulacaoResposta(1000.0);
        Mockito.when(simulacaoService.realizarSimulacao(Mockito.any(SimulacaoCadastro.class))).thenReturn(simulacaoResposta);
        String json = mapper.writeValueAsString(simulacaoResposta);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultadoSimulacao", CoreMatchers.equalTo(1000.0)));
    }

    @Test
    public void deveRealizarSimulacaoComErro() throws Exception {
        SimulacaoResposta simulacaoResposta = new SimulacaoResposta(1000.0);
        Mockito.when(simulacaoService.realizarSimulacao(Mockito.any(SimulacaoCadastro.class))).thenThrow(RuntimeException.class);
        String json = mapper.writeValueAsString(simulacaoResposta);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
