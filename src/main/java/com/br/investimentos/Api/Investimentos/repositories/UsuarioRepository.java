package com.br.investimentos.Api.Investimentos.repositories;

import com.br.investimentos.Api.Investimentos.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Usuario findByEmail(String email);
}
